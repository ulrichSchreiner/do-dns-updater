FROM registry.gitlab.com/ulrichschreiner/base/ubuntu:22.04

ENV DOCTL_VERSION=1.72.0

RUN apt update && apt -y install \
        curl \
        webhook \
        wget \
    && curl -SsL https://github.com/digitalocean/doctl/releases/download/v$DOCTL_VERSION/doctl-$DOCTL_VERSION-linux-amd64.tar.gz >/tmp/doctl.tgz \
    && cd tmp && tar xzvf /tmp/doctl.tgz \
    && mv /tmp/doctl /usr/bin/doctl \
    && rm -rf /tmp/* \
    && rm -rf /var/cache/apt/* \
    && rm -rf /var/lib/apt/*


COPY do-dns-update.sh /usr/bin/do-dns-update.sh

ENTRYPOINT ["/usr/bin/webhook"]