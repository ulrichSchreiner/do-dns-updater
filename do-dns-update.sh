#!/bin/bash

set -euxo pipefail

# make sure your DIGITALOCEAN_ACCESS_TOKEN is set in  the environment

MYIP=`/usr/bin/curl -SsL http://ifconfig.me/ip`
REC_NAME=$1
REC_TTL=$2
REC_TYPE=$3
REC_ID=$4
DOMAIN=$5

echo $MYIP

doctl compute domain records update --record-name $REC_NAME --record-ttl $REC_TTL --record-data $MYIP --record-type $REC_TYPE --record-id $REC_ID $DOMAIN